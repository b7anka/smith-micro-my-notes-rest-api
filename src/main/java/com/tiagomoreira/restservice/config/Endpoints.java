package com.tiagomoreira.restservice.config;

public class Endpoints {

    final static public String register = "/register";
    final static public String login = "/login";
    final static public String logout = "/logout";
    final static public String users = "/users";
    final static public String note = "/note";

}
