package com.tiagomoreira.restservice.config;

public class TranslationKeys {

    final static public String apiKeyMissing = "api_key_or_token_missing_or_wrong";
    final static public String emailOrPasswordWrong = "email_or_password_dont_match";
    final static public String userDoesNotExist = "user_does_not_exist";
    final static public String userAlreadyExists = "user_already_exists";
    final static public String noPermissionToEditNote = "no_permission_edit_note";
    final static public String noteDeletedSuccess = "note_deleted_success";
    final static public String logoutSuccess = "logout_success";
    final static public String logoutFailed = "logout_failed";
    final static public String registeredSuccessfully = "registered_successfully";
    final static public String cannotDeleteNote = "cannot_delete_note";
    final static public String userInvalid = "user_invalid";
    final static public String qrCodeInvalid = "qr_code_invalid";
    final static public String youAlreadyHaveThatNote = "you_already_have_that_note";

}
