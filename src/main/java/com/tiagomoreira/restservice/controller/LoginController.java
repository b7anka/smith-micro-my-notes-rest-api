package com.tiagomoreira.restservice.controller;

import com.tiagomoreira.restservice.config.Constants;
import com.tiagomoreira.restservice.config.Endpoints;
import com.tiagomoreira.restservice.config.HeaderKeys;
import com.tiagomoreira.restservice.config.TranslationKeys;
import com.tiagomoreira.restservice.entity.User;
import com.tiagomoreira.restservice.models.Login;
import com.tiagomoreira.restservice.models.LoginResponse;
import com.tiagomoreira.restservice.repository.UserRepository;
import com.tiagomoreira.restservice.utilities.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;
import java.util.Optional;

@RestController
@RequestMapping(path = Endpoints.login)
public class LoginController {

    @Autowired
    private UserRepository repository;

    @PostMapping(consumes = Constants.jsonContentType)
    public LoginResponse login(@RequestHeader(name = HeaderKeys.apiKey) String apiKey, @RequestBody Login user) throws UsernameNotFoundException {
        final Boolean apiKeyValid = Utils.shared.checkIfApiKeyIsValid(apiKey);
        if (!apiKeyValid) {
            throw new UsernameNotFoundException(TranslationKeys.apiKeyMissing);
        }
        final Optional<User> userToLogin = this.repository.findByEmail(user.getEmail());
        if (!userToLogin.isPresent()) {
            throw new UsernameNotFoundException(TranslationKeys.userDoesNotExist);
        }
        final Boolean matches = new BCryptPasswordEncoder().matches(user.getPassword(), userToLogin.get().getPassword());
        if (!matches) {
            throw new UsernameNotFoundException(TranslationKeys.emailOrPasswordWrong);
        }
        final String token = Utils.shared.getRandomToken();
        userToLogin.get().setToken(token);
        this.repository.save(userToLogin.get());
        LoginResponse response = new LoginResponse();
        response.setToken(token);
        response.setUser(userToLogin.get());
        return response;
    }

}
