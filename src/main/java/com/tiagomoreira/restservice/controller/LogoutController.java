package com.tiagomoreira.restservice.controller;

import com.tiagomoreira.restservice.config.Constants;
import com.tiagomoreira.restservice.config.Endpoints;
import com.tiagomoreira.restservice.config.HeaderKeys;
import com.tiagomoreira.restservice.config.TranslationKeys;
import com.tiagomoreira.restservice.entity.User;
import com.tiagomoreira.restservice.models.ServerMessage;
import com.tiagomoreira.restservice.repository.UserRepository;
import com.tiagomoreira.restservice.utilities.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping(path = Endpoints.logout)
public class LogoutController {

    @Autowired
    private UserRepository repository;

    @GetMapping(consumes = Constants.jsonContentType)
    public ServerMessage logout(@RequestHeader(name = HeaderKeys.apiKey) String apiKey, @RequestHeader(name = HeaderKeys.token) String token) {
        Boolean informationIsValid = Utils.shared.checkIfTokenAndApiKeyAreValid(apiKey, token, repository);
        if (informationIsValid) {
            final Optional<User> user = this.repository.findByToken(token);
            if (user.isPresent()) {
                user.get().setToken(null);
                this.repository.save(user.get());
                return new ServerMessage(TranslationKeys.logoutSuccess, Constants.typeSuccess, true);
            }
        }
        return new ServerMessage(TranslationKeys.logoutFailed, Constants.typeError, false);
    }

}
