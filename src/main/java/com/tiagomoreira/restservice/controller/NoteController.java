package com.tiagomoreira.restservice.controller;

import com.tiagomoreira.restservice.config.Constants;
import com.tiagomoreira.restservice.config.Endpoints;
import com.tiagomoreira.restservice.config.HeaderKeys;
import com.tiagomoreira.restservice.config.TranslationKeys;
import com.tiagomoreira.restservice.entity.Note;
import com.tiagomoreira.restservice.entity.User;
import com.tiagomoreira.restservice.models.AddByQrCodeModel;
import com.tiagomoreira.restservice.models.ServerMessage;
import com.tiagomoreira.restservice.repository.NoteRepository;
import com.tiagomoreira.restservice.repository.UserRepository;
import com.tiagomoreira.restservice.utilities.Utils;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = Endpoints.note)
public class NoteController {

    @Autowired
    private UserRepository repository;

    @Autowired
    private NoteRepository noteRepository;

    @GetMapping
    public List<Note> findAll(@RequestHeader(name = HeaderKeys.apiKey) String apiKey, @RequestHeader(name = HeaderKeys.token) String token) {
        Utils.shared.checkIfTokenAndApiKeyAreValid(apiKey, token, repository);
        final Optional<User> user = repository.findByToken(token);
        if (user.isPresent()) {
            final Iterable<Note> allNotes = noteRepository.findAll();
            List<Note> notesToReturn = new ArrayList<>();
            for (Note note: allNotes) {
                if (note.getOwnerId().equals(user.get().getId().toString()) || note.getSharedUsers() != null && note.getSharedUsers().contains(user.get())) {
                    notesToReturn.add(note);
                }
            }
            return notesToReturn;
        }
        return new ArrayList<>();
    }

    @PostMapping(consumes = Constants.jsonContentType)
    public Note createNote(@RequestHeader(name = HeaderKeys.apiKey) String apiKey, @RequestHeader(name = HeaderKeys.token) String token, @RequestBody Note note) {
        Utils.shared.checkIfTokenAndApiKeyAreValid(apiKey, token, repository);
        final Optional<User> user = repository.findByToken(token);
        long timestamp = System.currentTimeMillis();
        note.setDate(Long.toString(timestamp));
        user.ifPresent(value -> note.setOwnerId(value.getId().toString()));
        return noteRepository.save(note);
    }

    @PutMapping(consumes = Constants.jsonContentType)
    public Note updateNote(@RequestHeader(name = HeaderKeys.apiKey) String apiKey, @RequestHeader(name = HeaderKeys.token) String token, @RequestBody Note note) throws UsernameNotFoundException {
        Utils.shared.checkIfTokenAndApiKeyAreValid(apiKey, token, repository);
        Note noteToBeEdited = noteRepository.findOne(note.getId());
        final Optional<User> user = repository.findByToken(token);
        if (user.isPresent()){
            if (noteToBeEdited.getOwnerId().equals(user.get().getId().toString()) || noteToBeEdited.getSharedUsers() != null && noteToBeEdited.getSharedUsers().contains(user.get())) {
                noteToBeEdited.setName(note.getName());
                noteToBeEdited.setImageBase64(note.getImageBase64());
                noteToBeEdited.setContent(note.getContent());
                return noteRepository.save(noteToBeEdited);
            }
        }
        throw new UsernameNotFoundException(TranslationKeys.noPermissionToEditNote);
    }

    @PatchMapping(consumes = Constants.jsonContentType)
    public Note shareNote(@RequestHeader(name = HeaderKeys.apiKey) String apiKey, @RequestHeader(name = HeaderKeys.token) String token, @RequestBody AddByQrCodeModel qrCodeValue) throws UsernameNotFoundException {
        Utils.shared.checkIfTokenAndApiKeyAreValid(apiKey, token, repository);
        if (!qrCodeValue.getQrCodeData().contains(Constants.qrCodeKey)) {
            throw new UsernameNotFoundException(TranslationKeys.qrCodeInvalid);
        }
        final String qrCodeValueWithoutKey = qrCodeValue.getQrCodeData().replace(Constants.qrCodeKey, "");
        final String[] stringComponents = qrCodeValueWithoutKey.split("-");
        if (stringComponents.length != 2) {
            throw new UsernameNotFoundException(TranslationKeys.qrCodeInvalid);
        }
        byte[] bytesEncodedNoteID = stringComponents[0].getBytes(StandardCharsets.UTF_8);
        byte[] bytesEncodedOwnerID = stringComponents[1].getBytes(StandardCharsets.UTF_8);
        byte[] valueDecodedNoteId = Base64.decodeBase64(bytesEncodedNoteID);
        byte[] valueDecodedOwnerId = Base64.decodeBase64(bytesEncodedOwnerID);
        final String noteIdDecoded = new String(valueDecodedNoteId);
        final String ownerIdDecoded = new String(valueDecodedOwnerId);
        final long noteId = Long.parseLong(noteIdDecoded);
        final long ownerId = Long.parseLong(ownerIdDecoded);
        Note noteToBeShared = noteRepository.findOne(noteId);
        Optional<User> userToShareWith = repository.findByToken(token);
        if (userToShareWith.isPresent() && userToShareWith.get().getId().equals(ownerId)) {
            throw new UsernameNotFoundException(TranslationKeys.youAlreadyHaveThatNote);
        } else if (!userToShareWith.isPresent()) {
            throw new UsernameNotFoundException(TranslationKeys.qrCodeInvalid);
        }
        final List<Note> usersToShareWithNotes = new ArrayList<>();
        for (Note n : noteRepository.findAll()) {
            if (n.getOwnerId().equals(userToShareWith.get().getId().toString()) || n.getSharedUsers() != null && n.getSharedUsers().contains(userToShareWith.get())) {
                usersToShareWithNotes.add(n);
            }
        }
        if (usersToShareWithNotes.contains(noteToBeShared)) {
            throw new UsernameNotFoundException(TranslationKeys.youAlreadyHaveThatNote);
        }
        List<User> usersOnNoteToShare = noteToBeShared.getSharedUsers() != null ? noteToBeShared.getSharedUsers() : new ArrayList<>();
        usersOnNoteToShare.add(userToShareWith.get());
        noteToBeShared.setSharedUsers(usersOnNoteToShare);
        return this.noteRepository.save(noteToBeShared);
    }

    @DeleteMapping(consumes = Constants.jsonContentType)
    public ServerMessage deleteNote (@RequestHeader(name = HeaderKeys.apiKey) String apiKey, @RequestHeader(name = HeaderKeys.token) String token, @RequestBody Note note) {
        Utils.shared.checkIfTokenAndApiKeyAreValid(apiKey, token, repository);
        final Optional<User> user = repository.findByToken(token);
        final Note noteToBeDeleted = noteRepository.findOne(note.getId());
        if (user.isPresent() && noteToBeDeleted.getOwnerId().equals(user.get().getId().toString())) {
            noteRepository.delete(noteToBeDeleted.getId());
            return new ServerMessage(TranslationKeys.noteDeletedSuccess, Constants.typeSuccess, true);
        }
        return new ServerMessage(TranslationKeys.cannotDeleteNote, Constants.typeError, false);
    }

}
