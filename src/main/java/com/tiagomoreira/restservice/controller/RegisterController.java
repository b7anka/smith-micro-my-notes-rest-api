package com.tiagomoreira.restservice.controller;

import com.tiagomoreira.restservice.config.Constants;
import com.tiagomoreira.restservice.config.Endpoints;
import com.tiagomoreira.restservice.config.HeaderKeys;
import com.tiagomoreira.restservice.config.TranslationKeys;
import com.tiagomoreira.restservice.entity.User;
import com.tiagomoreira.restservice.models.Register;
import com.tiagomoreira.restservice.models.ServerMessage;
import com.tiagomoreira.restservice.repository.UserRepository;
import com.tiagomoreira.restservice.utilities.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping(path = Endpoints.register)
public class RegisterController {

    @Autowired
    private UserRepository repository;

    @PostMapping(consumes = Constants.jsonContentType)
    public ServerMessage register(@RequestHeader(name = HeaderKeys.apiKey) String apiKey, @RequestBody Register user) throws UsernameNotFoundException {
        final Boolean apiKeyValid = Utils.shared.checkIfApiKeyIsValid(apiKey);
        if (!apiKeyValid) {
            throw new UsernameNotFoundException(TranslationKeys.apiKeyMissing);
        }
        final Optional<User> existUser = this.repository.findByEmail(user.getBasicData().getEmail());
        if (existUser.isPresent()) {
            throw new UsernameNotFoundException(TranslationKeys.userAlreadyExists);
        }
        final String encryptedPassword = new BCryptPasswordEncoder().encode(user.getBasicData().getPassword());
        final User createUser = new User();
        createUser.setPassword(encryptedPassword);
        createUser.setEmail(user.getBasicData().getEmail());
        createUser.setImageBase64(user.getImageBase64());
        this.repository.save(createUser);
        return new ServerMessage(TranslationKeys.registeredSuccessfully, Constants.typeSuccess, true);
    }

}
