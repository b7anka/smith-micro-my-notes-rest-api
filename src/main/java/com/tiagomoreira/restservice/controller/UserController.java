package com.tiagomoreira.restservice.controller;

import com.tiagomoreira.restservice.config.Endpoints;
import com.tiagomoreira.restservice.config.HeaderKeys;
import com.tiagomoreira.restservice.entity.User;
import com.tiagomoreira.restservice.repository.UserRepository;
import com.tiagomoreira.restservice.utilities.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = Endpoints.users)
public class UserController {

    @Autowired
    private UserRepository repository;

    @GetMapping
    public Iterable<User> findAll(@RequestHeader(name = HeaderKeys.apiKey) String apiKey, @RequestHeader(name = HeaderKeys.token) String token) throws UsernameNotFoundException {
        Utils.shared.checkIfTokenAndApiKeyAreValid(apiKey, token, repository);
        return this.repository.findAll();
    }

    @GetMapping(path = "/{id}")
    public User find(@RequestHeader(name = HeaderKeys.apiKey) String apiKey, @RequestHeader(name = HeaderKeys.token) String token, @PathVariable("id") Long id) {
        Utils.shared.checkIfTokenAndApiKeyAreValid(apiKey, token, repository);
        return this.repository.findOne(id);
    }

    @DeleteMapping(path = "/{id}")
    public void delete(@RequestHeader(name = HeaderKeys.apiKey) String apiKey, @RequestHeader(name = HeaderKeys.token) String token, @PathVariable("id") Long id) {
        Utils.shared.checkIfTokenAndApiKeyAreValid(apiKey, token, repository);
        this.repository.delete(id);
    }

}
