package com.tiagomoreira.restservice.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.validator.constraints.NotEmpty;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;

@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @NotEmpty
    private String email;
    @NotEmpty
    private String password;
    @Nullable
    @Column(columnDefinition="LONGTEXT")
    private String imageBase64;
    @Nullable
    private String token;

    public Long getId() {
        return this.id;
    }

    public String getEmail() {
        return this.email;
    }

    @JsonIgnore
    public String getToken() {
        return this.token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @JsonIgnore
    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getImageBase64() {
        return this.imageBase64;
    }

    public void setImageBase64(String image) {
        this.imageBase64 = image;
    }

    @Override
    public String toString() {
        return "User{" +
                "email='" + email + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
