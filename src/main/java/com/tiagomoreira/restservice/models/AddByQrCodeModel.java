package com.tiagomoreira.restservice.models;

public class AddByQrCodeModel {

    private String qrCodeData;

    public String getQrCodeData() {
        return qrCodeData;
    }

    public void setQrCodeData(String qrCodeData) {
        this.qrCodeData = qrCodeData;
    }

}
