package com.tiagomoreira.restservice.models;

public class Login {

    private String email;
    private String password;

    public String getPassword() {
        return this.password;
    }

    public String getEmail() {
        return this.email;
    }
}

