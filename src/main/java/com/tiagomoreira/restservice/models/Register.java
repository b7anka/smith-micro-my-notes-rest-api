package com.tiagomoreira.restservice.models;

public class Register {

    private Login basicData;
    private String imageBase64;

    public Login getBasicData() {
        return this.basicData;
    }

    public void setBasicData(Login basicData) {
        this.basicData = basicData;
    }

    public String getImageBase64() {
        return imageBase64;
    }

    public void setImageBase64(String imageBase64) {
        this.imageBase64 = imageBase64;
    }
}
