package com.tiagomoreira.restservice.models;

public class ServerMessage {

    private final String message;
    private final String type;
    private final Boolean success;

    public ServerMessage(String message, String type, Boolean success) {
        this.message = message;
        this.type = type;
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public Boolean getSuccess() {
        return success;
    }

    public String getType() {
        return type;
    }
}
