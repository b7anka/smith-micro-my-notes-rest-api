package com.tiagomoreira.restservice.repository;

import com.tiagomoreira.restservice.entity.Note;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RestResource;

@RestResource(exported = false)
public interface NoteRepository extends JpaRepository<Note, Long> {

}
