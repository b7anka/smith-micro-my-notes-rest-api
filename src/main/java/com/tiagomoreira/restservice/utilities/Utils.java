package com.tiagomoreira.restservice.utilities;

import com.tiagomoreira.restservice.config.Constants;
import com.tiagomoreira.restservice.config.TranslationKeys;
import com.tiagomoreira.restservice.entity.User;
import com.tiagomoreira.restservice.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;
import java.util.UUID;

public class Utils {

    final public static Utils shared = new Utils();

    public String getRandomToken() {
        return UUID.randomUUID().toString().replace("-", "");
    }

    public Boolean checkIfTokenAndApiKeyAreValid(String apiKey, String token, UserRepository repository) throws UsernameNotFoundException {
        if (!this.checkIfApiKeyIsValid(apiKey)) {
            throw new UsernameNotFoundException(TranslationKeys.apiKeyMissing);
        }else if (!this.checkIfTokenIsValid(token, repository)) {
            throw new UsernameNotFoundException(TranslationKeys.userInvalid);
        }
        return true;
    }

    public Boolean checkIfApiKeyIsValid(String apiKey) {
        if (apiKey.isEmpty()) {
            return false;
        }else {
            if (!apiKey.equals(Constants.apiKey)) {
                return false;
            }
        }
        return true;
    }

    private Boolean checkIfTokenIsValid(String token, UserRepository repository) {
        final Optional<User> user = repository.findByToken(token);
        if (!user.isPresent()) {
            return false;
        }
        return true;
    }

}
